<x-app-layout>

    <link rel="stylesheet" href="{{asset('../../../assets/css/facility.css')}}">
    <h1>Facilities</h1>
    <!-- Display Success Message -->
    @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <!-- Display Error Messages -->
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="maincontainer">
        <div class="row">
            <div class="col-lg-8 p-3">
                <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Add</button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Add Facility</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('facilities.create')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label text-secondary">Facility Name</label>
                                        <input name="facility_name" type="text" class="form-control border-dark" id="recipient-name">
                                    </div>
                                    <div class="mb-3">
                                        <label for="message-text" class="col-form-label text-secondary">Description</label>
                                        <textarea name="desc" class="form-control border-dark" id="message-text"></textarea>
                                    </div>
                                    <div>
                                        <label for="recipient-name" class="col-form-label text-secondary">Time</label>
                                    </div>

                                    <div class="mb-3 form-check-inline" style="display:flex;flex-direction:column">
                                        <div class="input-group">
                                            <input class="form-control" name="starttime" type="time" value="00:00">
                                            <input class="form-control" name="endtime" type="time" value="00:00">
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="message-text" class="col-form-label text-secondary">Time Slot</label>
                                        <fieldset class="input-group">
                                            <input class="form-control" type="number" name="hour" value="0" aria-label="hour" aria-describedby="hour-description" min="0" max="24" placeholder="hour" />
                                            <span class="input-group-text">:</span>
                                            <input class="form-control" type="number" name="minute" value="0" aria-label="minute" min="0" max="59" placeholder="minute" />
                                        </fieldset>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input name="role" class="form-check-input border-dark" type="checkbox" value="all" id="flexCheckDefault">
                                        <label class="form-check-label text-secondary" for="flexCheckDefault">
                                            All
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input name="role" class="form-check-input border-dark" type="checkbox" value="staff" id="flexCheckChecked">
                                        <label class="form-check-label text-secondary" for="flexCheckChecked">
                                            Only Staff
                                        </label>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input name="image" accept="image/jpeg, image/png" type="file" class="form-control border-dark" id="inputGroupFile01">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input border-dark" type="radio" name="status" value="available" id="flexRadioDefault1">
                                        <label class="form-check-label text-success" for="flexRadioDefault1">
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input border-dark" type="radio" name="status" value="unavailable" id="flexRadioDefault2">
                                        <label class="form-check-label text-danger" for="flexRadioDefault2">
                                            Unavailable
                                        </label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-3">
                <div class="input-group input-group mb-3">
                    <input type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search">
                    <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                </div>
                <!-- <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div> -->
            </div>
        </div>

        <div class=" table-responsive p-3">
            <table class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th scope="col" style="color:white; padding-Left:5px">Image</th>
                        <th scope="col" style="color:white; padding-Left:5px">Facility</th>

                        <th scope="col" style="color:white; padding-Left:5px">Descriptions</th>
                        <th scope="col" style="color:white; padding-Left:5px">status</th>
                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $sl = 1;  
                    @endphp
                    @foreach ($facilities as $facility)
                        
                    
                    <tr>
                        <th scope="row">{{$sl}}</th>
                        <td><img src="{{ Storage::url($facility->image) }}" width="70" height="50"></td>
                        <td style="font-size:12px; padding-Left:5px">{{$facility->facility_name}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$facility->desc}}</td>
                        <td style="color:#27A806; font-size:12px; padding-Left:5px">{{$facility->status}}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Edit</button>
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Add Facility</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label text-secondary">Facility Name</label>
                                                    <input type="text" class="form-control border-dark" id="recipient-name" value="{{$facility->facility_name}}">
                                                </div>
                                                <div class="mb-3">
                                                    <label for="message-text" class="col-form-label text-secondary">Description</label>
                                                    <textarea class="form-control border-dark" id="message-text"></textarea>
                                                </div>
                                                <div>
                                                    <label for="recipient-name" class="col-form-label text-secondary">timer</label>
                                                </div>

                                                <div class="mb-3 form-check-inline">
                                                    <input type="checkbox" name="time[]" value="item1"> Item 1<br>
                                                    <input type="checkbox" name="time[]" value="item2"> Item 2<br>
                                                    <input type="checkbox" name="time[]" value="item3"> Item 3<br>

                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="checkbox" value="" id="flexCheckDefault">
                                                    <label class="form-check-label text-secondary" for="flexCheckDefault">
                                                        Students
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="checkbox" value="" id="flexCheckChecked">
                                                    <label class="form-check-label text-secondary" for="flexCheckChecked">
                                                        Staffs
                                                    </label>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="file" class="form-control border-dark" id="inputGroupFile01">
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                                    <label class="form-check-label text-success" for="flexRadioDefault1">
                                                        Available
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                                    <label class="form-check-label text-danger" for="flexRadioDefault2">
                                                        Unavailable
                                                    </label>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @php
                    $sl++
                    @endphp
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination ">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>
        <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</x-app-layout>