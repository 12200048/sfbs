<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/feedback.css')}}">
    <h1>Feedbacks</h1>
    <div class="maincontainer">
        <!-- <div class="row">
            <p>User Feedbacks</p>
        </div> -->
        <div class=" table-responsive">
            <table class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th scope="col" style="color:white; padding-Left:5px">Feedbacks</th>
                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($feedbacks as $feedback)
                    <tr>
                        <th scope="row">1</th>
                        <td style=" font-size:12px; padding-Left:5px">{{$feedback->content}}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Delete
                            </button>

                            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Delete feedback</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this Feedback?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <form action="{{ route('user.deleteFeedback', $feedback->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button style="color:rgb(218, 64, 64)" type="submit" class="btn btn-primary">Yes</button>
                                            </form>                                        
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination ">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</x-app-layout>