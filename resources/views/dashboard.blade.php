<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/dashboard.css')}}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="he">DashBoard</h1>
            </div>
        </div>

        <div class="row g-4 mb-4">
            <div class="col-lg-8" style="min-height: 600px;">
                <div class="maincontainer table-responsive p-3">
                    <table class="table-info w-100">
                        <thead>
                            <tr>
                                <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                                <th scope="col" style="color:white; padding-Left:5px">Image</th>
                                <th scope="col" style="color:white; padding-Left:5px">Facility</th>
                                <th scope="col" style="color:white; padding-Left:5px">status</th>
                                <th scope="col" style="color:white; padding-Left:5px">Descriptions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $serialNumber = 1; // Initialize the serial number counter
                            @endphp
                            @foreach ($facilities as $facility)
                                <tr>
                                    <th scope="row">{{$serialNumber}}</th>
                                    <td><img src="{{ asset('storage/' . $facility->image) }}" width="70" height="50"></td>
                                    <td style="font-size:12px; padding-Left:5px">{{$facility->facility_name}}</td>
                                    @if ($facility->status === 'Available')
                                        <td style="color:#27A806; font-size:12px; padding-Left:5px">Avaliable</td>
                                    @else
                                        <td style="color:red; font-size:12px; padding-Left:5px">Unavailable</td>
                                    @endif
                                    <td style="font-size:12px;padding-Left:5px">{{$facility->desc}}</td>
                                </tr>
                                @php
                                    $serialNumber++; // Increment the serial number for the next iteration
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination ">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="maincontainer table-responsive mt-4 p-3">

                    <table class="table-info w-100">
                        <thead>
                            <tr>
                                <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                                <th scope="col" style="color:white; padding-Left:5px">Employ ID</th>
                                <th scope="col" style="color:white; padding-Left:5px">Facility</th>
                                <th scope="col" style="color:white; padding-Left:5px">Date</th>
                                <th scope="col" style="color:white; padding-Left:5px">Time</th>
                                <th scope="col" style="color:white; padding-Left:5px">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $serialNumber = 1; // Initialize the serial number counter
                            @endphp
                            @foreach ($bookings as $booking)
                                <tr>
                                    <th scope="row">{{$serialNumber}}</th>
                                    <td style="font-size:12px; padding-Left:5px">{{ $booking->users->employeeid}}</td>
                                    <td style="font-size:12px; padding-Left:5px">Football Ground</td>
                                    <td style="color:#27A806; font-size:12px; padding-Left:5px">07/03/2024</td>
                                    <td style="font-size:12px;padding-Left:5px">7:00am-8:00am</td>
                                    <td style="font-size:12px;padding-Left:5px">Pending</td>
                                </tr>
                                @php
                                    $serialNumber++; // Increment the serial number for the next iteration
                                @endphp
                            @endforeach
                            
                           
                        </tbody>
                    </table>

                    <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination ">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 d-flex flex-column gap-4" style="min-height: 600px;">
                <div class="states h-25 p-3 text-white">
                    <h6 class="">Sherubtse College<br>
                        <a class="text-white" style="font-size: 10px;">https://www.sherubtse.edu.bt/</a>
                    </h6>
                    <div class="d-flex mt-4">
                        <h6>{{$rowCount}} <br>Facilities</h6>
                        <h6 class="ms-3">{{$countBook}} <br>Booking</h6>
                    </div>
                    <div class="text-center" style="margin-left:160px; margin-top:-65px">
                        <h6>{{$totalUsers}}</h6>
                        <h6>Users</h6>
                    </div>

                </div>
                <div class="h-75 maincontainer">
                    <div class="pt-3 px-3 mb-4">
                        <input class="form-control" type="number" min="2020" max="2024" step="1" value="2024" />

                    </div>
                    <div class="text-center my-2 mt-5">
                        <h5>facility</h5>
                    </div>
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
        // Data for the pie chart
        const chartData = {
            series: [44, 55, 13],
            labels: ['Football Ground', 'Basketball Court', 'MPH']
        };

        // Configuration for the pie chart
        const chartOptions = {
            chart: {
                type: 'pie'
            },
            series: chartData.series,
            labels: chartData.labels,
            // Legend configuration
            legend: {
                position: 'top',
                horizontalAlign: 'left',
                formatter: function(seriesName) {
                    return seriesName + "<br>";
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        };

        // Initialize the chart
        const chart = new ApexCharts(document.querySelector("#chart"), chartOptions);

        // Render the chart
        chart.render();
    </script>

</x-app-layout>