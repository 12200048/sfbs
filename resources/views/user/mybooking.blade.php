<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/homepage.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <img class="rlogo" src="{{asset('img/logo.png')}}" /> 
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link "  href="{{route('user.homepage')}}">FACILITIES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="{{route('user.mybooking')}}">MY BOOKINGS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user.feedback')}}">FEEDBACKS</a>
          </li>
        
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" width="30px" height="30" type="submit">
            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M11 2a9 9 0 1 0 5.618 16.032l3.675 3.675a1 1 0 0 0 1.414-1.414l-3.675-3.675A9 9 0 0 0 11 2m-6 9a6 6 0 1 1 12 0a6 6 0 0 1-12 0" clip-rule="evenodd"/></svg>
          </button>
        </form>
        <form class="d-flex">
          <svg cursor="pointer" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24"><path fill="currentColor" d="M4 19v-2h2v-7q0-2.075 1.25-3.687T10.5 4.2v-.7q0-.625.438-1.062T12 2t1.063.438T13.5 3.5v.7q2 .5 3.25 2.113T18 10v7h2v2zm8 3q-.825 0-1.412-.587T10 20h4q0 .825-.587 1.413T12 22m-4-5h8v-7q0-1.65-1.175-2.825T12 6T9.175 7.175T8 10z"/></svg>
          <div class="flex items-center">
            <div class="flex items-center ms-3">
              <div>
                <button type="button" class="flex text-sm rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" aria-expanded="false" data-dropdown-toggle="dropdown-user">
                  <span class="sr-only">Open user menu</span>
                  <div class="flex items-center">
                    <div class="flex items-center ms-3">
                      <div>
                        <button type="button" class="flex text-sm rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" aria-expanded="false" data-dropdown-toggle="dropdown-user">
                          <span class="sr-only">Open user menu</span>
                          <svg xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M12 4a8 8 0 0 0-6.96 11.947A4.99 4.99 0 0 1 9 14h6a4.99 4.99 0 0 1 3.96 1.947A8 8 0 0 0 12 4m7.943 14.076A9.959 9.959 0 0 0 22 12c0-5.523-4.477-10-10-10S2 6.477 2 12a9.958 9.958 0 0 0 2.057 6.076l-.005.018l.355.413A9.98 9.98 0 0 0 12 22a9.947 9.947 0 0 0 5.675-1.765a10.055 10.055 0 0 0 1.918-1.728l.355-.413zM12 6a3 3 0 1 0 0 6a3 3 0 0 0 0-6" clip-rule="evenodd"/></svg>
                        </button>
                      </div>
                      <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600" id="dropdown-user">
                        <ul class="py-1" role="none">
                          <li>
                            <a href="{{route('user.profile')}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white" role="menuitem">Profile</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>                </button>
              </div>
              <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600" id="dropdown-user">
                <div class="px-4 py-3" role="none">
                  <p class="text-sm text-gray-900 dark:text-white" role="none">
                    {{ Auth::user()->name }}
                  </p>
                  <p class="text-sm font-medium text-gray-900 truncate dark:text-gray-300" role="none">
                    {{ Auth::user()->email }}
                  </p>
                </div>
                
              </div>
            </div>
          </div>
        </form>


      </div>
    </div>
  </nav>

  <div class="card1">
    <div class="card1-header">
        <h2>My Bookings</h2>
    </div>
    <div class="card1-body">
        <table>
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Image</th>
                    <th>Facility</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @php
                $serialNumber = 1; // Initialize the serial number counter
              @endphp
              @foreach ($bookings as $booking)
                <tr>
                    <td>{{ $serialNumber }}</td>
                    <td>
                      <img src="{{ asset('storage/' . $booking->facility->image) }}" width="60px" height="60px"/>
                    </td>
                    <td>{{ $booking->facility->facility_name }}</td>
                    <td>{{$booking->starttime}}</td>
                    <td>{{$booking->endtime}}</td>
                    <td>{{$booking->status}}</td>
                    <td>
                      <form action="{{ route('user.deleteBooking', $booking->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button style="color:rgb(218, 64, 64)" type="submit" onclick="return confirm('Are you sure you want to delete this item?')">Delete</button>
                    </form>
                    </td>
                </tr>
                @php
                  $serialNumber++; // Increment the serial number for the next iteration
                @endphp
                @endforeach
                
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <br><br><br>
    <p class="page" >< 1 ></p>
</div>

  <footer>
    <div class="footer-container">
        <div class="footer-section">
            <img src="{{asset('img/logo.png')}}" alt="Company Logo">
        </div>
        <div class="footer-section">
            <nav>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </nav>
        </div>
        <div class="footer-section">
            <p>&copy; 2024 Company Name</p>
            <p>123 Main Street, City, Country</p>
        </div>
    </div>
    <hr class="footer-separator">
    <p class="t" >COPYRIGHTS SHERUBTSE COLLEGE 2024</p>
</footer>
<script src="../path/to/flowbite/dist/flowbite.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
</body>
</html>