<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/homepage.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Profile</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <img class="rlogo" src="{{asset('img/logo.png')}}" /> 
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active"  href="{{route('user.homepage')}}">FACILITIES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user.mybooking')}}">MY BOOKINGS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user.feedback')}}">FEEDBACKS</a>
          </li>
        
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" width="30px" height="30" type="submit">
            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M11 2a9 9 0 1 0 5.618 16.032l3.675 3.675a1 1 0 0 0 1.414-1.414l-3.675-3.675A9 9 0 0 0 11 2m-6 9a6 6 0 1 1 12 0a6 6 0 0 1-12 0" clip-rule="evenodd"/></svg>
          </button>
        </form>
        <form class="d-flex">
          <svg class="pro" cursor="pointer" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24"><path fill="currentColor" d="M4 19v-2h2v-7q0-2.075 1.25-3.687T10.5 4.2v-.7q0-.625.438-1.062T12 2t1.063.438T13.5 3.5v.7q2 .5 3.25 2.113T18 10v7h2v2zm8 3q-.825 0-1.412-.587T10 20h4q0 .825-.587 1.413T12 22m-4-5h8v-7q0-1.65-1.175-2.825T12 6T9.175 7.175T8 10z"/></svg>
                
          <div class="flex items-center">
            <div class="flex items-center ms-3">
              <div>
                <button type="button" class="flex text-sm rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" aria-expanded="false" data-dropdown-toggle="dropdown-user">
                  <span class="sr-only">Open user menu</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M12 4a8 8 0 0 0-6.96 11.947A4.99 4.99 0 0 1 9 14h6a4.99 4.99 0 0 1 3.96 1.947A8 8 0 0 0 12 4m7.943 14.076A9.959 9.959 0 0 0 22 12c0-5.523-4.477-10-10-10S2 6.477 2 12a9.958 9.958 0 0 0 2.057 6.076l-.005.018l.355.413A9.98 9.98 0 0 0 12 22a9.947 9.947 0 0 0 5.675-1.765a10.055 10.055 0 0 0 1.918-1.728l.355-.413zM12 6a3 3 0 1 0 0 6a3 3 0 0 0 0-6" clip-rule="evenodd"/></svg>
                </button>
              </div>
              <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600" id="dropdown-user">
                <ul class="py-1" role="none">
                  <li>
                    <a href="{{route('user.profile')}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white" role="menuitem">Profile</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </form>

      </div>
    </div>
  </nav>

  <section style="padding-block: 30px;display:flex;justify-content:space-evenly">
    <div class="card border-light mb-3" style="max-width: 18rem;height:33rem">
      <div class="card-header" style="font-weight:600;font-size:18px">User Details</div>
      <div class="card-body">
        <span style="font-weight:600">Username</span>
        <p class="card-text">{{ Auth::user()->name }}</p>
        <span style="font-weight:600">Email ID</span>
        <p class="card-text">{{ Auth::user()->email }}</p>
        <span style="font-weight:600">Employee ID</span>
        <p class="card-text">{{ Auth::user()->employeeid }}</p>
        <span style="font-weight:600">Contact No.</span>
        <p class="card-text">{{ Auth::user()->contact }}</p>
        <span style="font-weight:600">Password</span>
        <p class="card-text">{{ Auth::user()->password }}</p>
        <button type="button" class="btn btn-primary" style="width:100%;margin-bottom:20px" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit Profile</button>
        <form method="POST" action="{{ route('logout') }}" x-data>
          @csrf
          <button type="submit" class="btn btn-primary" style="width:100%">Logout</button>
        </form> 

      </div>
    </div>
    <div class="card border-light mb-3" style="max-width: 18rem;height:12rem">
      <div class="card-header" style="font-weight:600;font-size:18px">Account Deletion</div>
      <div class="card-body">
        <p class="card-text">Once you delete your account, there is no
          going back. Please be certain.
        </p>
        <form method="POST" action="{{ route('users.destroy', Auth::user()->id) }}" onsubmit="return confirm('Are you sure you want to delete your account. Once you deleteyour account, there is no going back. Please be certain')">
          @csrf
          @method('DELETE')
          <button type="submit" class="btn btn-primary" style="width:100%">Delete Account</button>
        </form>
      </div>
    </div>
    
  </section>


  <footer>
    <div class="footer-container">
        <div class="footer-section">
            <img src="{{asset('img/logo.png')}}" alt="Company Logo">
        </div>
        <div class="footer-section">
            <nav>
                <ul>
                    <li><a href="#">E-SERVICES</a></li>
                    <li>VLE</p></li>
                    <li><p>RUB IMS</p></li>
                    <li><p>Online Catalogue</p></li>
                  
                </ul>
            </nav>
        </div>
        <div class="footer-section">
             <p><a>QUICK-LINKS</a>  </p>
            <p>G2C Services</p>
            <p>MoICE</p>
        </div>
    </div>
    <hr class="footer-separator">
    <p class="t" >&copy; COPYRIGHTS SHERUBTSE COLLEGE 2024</p>
</footer>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('user.update', Auth::user()->id) }}">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Username:</label>
            <input value={{Auth::user()->name}} type="text" class="form-control sm" id="recipient-name" name="name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Email ID:</label>
            <input value={{Auth::user()->email}} type="text" class="form-control sm" id="recipient-name" name="email">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Employ ID:</label>
            <input value={{Auth::user()->employeeid}} type="text" class="form-control sm" id="recipient-name" name="employeeid">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Contact No:</label>
            <input value={{Auth::user()->contact}} type="text" class="form-control sm" id="recipient-name" name="contact">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Password:</label>
            <input value={{Auth::user()->password}} type="text" class="form-control sm" id="recipient-name" name="password">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="../path/to/flowbite/dist/flowbite.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>