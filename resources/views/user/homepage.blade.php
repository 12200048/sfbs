<x-user-layout>

  <p class= "name">WELCOME BACK {{ Auth::user()->name }}</p>

  <div class="container" >
    @foreach ($facilities as $facility)
      <div class="card">
        <img src="{{ Storage::url($facility->image) }}" alt="Image" class="card-image">
        <div class="card-content">
            <h3 class="card-title">{{$facility->facility_name}}</h3>
            <p class="card-text">{{$facility->desc}}</p>
            <a href="{{ route('user.bookingpage', ['id' => $facility->id]) }}" class="btnn">BOOK NOW</a>      
          </div>
      </div>
    @endforeach
  {{-- <div class="card">
    <img src="{{asset('img/football.png')}}" alt="Image" class="card-image">
    <div class="card-content">
        <h3 class="card-title">Football Ground</h3>
        <p class="card-text">This is the facility that we have in our college. Students are free to booking as per their planned time, from anywhere,any time.</p>
        <a href="#" class="btnn">BOOK NOW</a>
    </div>
  </div> --}}

  {{-- <div class="card">
    <img src="{{asset('img/mph.png')}}" alt="Image" class="card-image">
    <div class="card-content">
        <h3 class="card-title">MPH</h3>
        <p class="card-text">This is the facility that we have in our college. Students are free to booking as per their planned time, from anywhere,any time.</p>
        <a href="#" class="btnn">BOOK NOW</a>
    </div>
  </div>
  </div> --}}

  {{-- <div class="container" >
  <div class="card">
    <img src="{{asset('img/basket.png')}}" alt="Image" class="card-image">
    <div class="card-content">
        <h3 class="card-title">Basketball Court</h3>
        <p class="card-text">This is the facility that we have in our college. Students are free to booking as per their planned time, from anywhere,any time.</p>
        <a href="#" class="btnn">BOOK NOW</a>
    </div>
  </div> --}}

  {{-- <div class="card">
    <img src="{{asset('img/football.png')}}" alt="Image" class="card-image">
    <div class="card-content">
        <h3 class="card-title">Football Ground</h3>
        <p class="card-text">This is the facility that we have in our college. Students are free to booking as per their planned time, from anywhere,any time.</p>
        <a href="#" class="btnn">BOOK NOW</a>
    </div>
  </div> --}}

  {{-- <div class="card">
    <img src="{{asset('img/mph.png')}}" alt="Image" class="card-image">
    <div class="card-content">
        <h3 class="card-title">MPH</h3>
        <p class="card-text">This is the facility that we have in our college. Students are free to booking as per their planned time, from anywhere,any time.</p>
        <a href="#" class="btnn">BOOK NOW</a>
    </div>
  </div> --}}
  </div>


</x-user-layout>

