<x-user-layout>
   
    <div class="cardb">
    <img src="{{ Storage::url($facility->image) }}" alt="Image" class="image">
    <div class="cardb-header">

    </div>
    <div class="card1-body">
        <p class="h">Facility: {{$facility->facility_name}}</p>
        <p class="h">Booking Time: {{$facility->starttime}} - {{$facility->endtime}}</p>
        <p class="h">Booking Duration: {{$facility->step}}</p>
        <form class="ff" action="{{route('user.bookingpage.create')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="facility_id" id="facility_id" value="{{ $facility->id }}">
            <label for="appointment-date">Select Date:</label>
            <input class="in" type="date" id="appointment-date" name="date" required min="{{ date('Y-m-d') }}">
            <label for="slot-container">Select Start Time:</label>
            <select id="slot-container" name="time" class="in text-sm" aria-label="Default select example" required>
                <option selected disabled>Select Date</option>
            </select>

            <label for="full-name">Reason for Booking</label>
            <input class="in" type="text" id="full-name" name="reason" required><br>

            <button type="submit" class="btn1">Submit</a>
        </form>
    </div>
</div>



{{-- <script>
    document.addEventListener("DOMContentLoaded", function() {
        var select = document.getElementById("slot-container");
        var selectDateInput = document.getElementById("appointment-date");
        var facilityStartTime = '{{ $facility->starttime }}';
        var facilityEndTime = '{{ $facility->endtime }}';
        var step = '{{ $facility->step }}'; // Step in H:i:s format (e.g., '00:30:00')
        var timeZone = 'Asia/Thimphu'; // Timezone for Bhutan (Asia/Thimphu)
        var bookingsStartTimes = @json($bookingsData);

        function generateTimeSlots() {
            select.innerHTML = '';

            var selectedDate = selectDateInput.value;
            var today = new Date(); // Current date and time
            var currentTime = new Date(today.toLocaleString('en-US', { timeZone: timeZone }));

            var startTime = new Date(selectedDate + 'T' + facilityStartTime);
            var endTime = new Date(selectedDate + 'T' + facilityEndTime);

            // Generate time slots
            var currentTimeIncremented = new Date(startTime);

            while (currentTimeIncremented < endTime) {
                var formattedTime = currentTimeIncremented.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true, timeZone: timeZone });

                // Skip displaying time slots that are in the past
                if (selectedDate === formatDate(today) && currentTimeIncremented < currentTime) {
                    currentTimeIncremented.setHours(currentTimeIncremented.getHours() + parseInt(step.substring(0, 2))); // Increment time
                    currentTimeIncremented.setMinutes(currentTimeIncremented.getMinutes() + parseInt(step.substring(3, 5)));
                    currentTimeIncremented.setSeconds(currentTimeIncremented.getSeconds() + parseInt(step.substring(6, 8)));
                    continue;
                }

                // Check if the time slot is already booked for the selected date
                var isBooked = isTimeBooked(selectedDate, formattedTime);

                // Only add the time slot if it's not booked
                if (!isBooked) {
                    var option = document.createElement('option');
                    option.text = formattedTime;
                    option.value = formattedTime;
                    select.appendChild(option);
                }

                // Increment currentTimeIncremented by the step
                currentTimeIncremented.setHours(currentTimeIncremented.getHours() + parseInt(step.substring(0, 2))); // Add hours from step
                currentTimeIncremented.setMinutes(currentTimeIncremented.getMinutes() + parseInt(step.substring(3, 5))); // Add minutes from step
                currentTimeIncremented.setSeconds(currentTimeIncremented.getSeconds() + parseInt(step.substring(6, 8))); // Add seconds from step
            }
        }

        function formatDate(date) {
            // Helper function to format date as YYYY-MM-DD
            var year = date.getFullYear();
            var month = (date.getMonth() + 1).toString().padStart(2, '0'); // Month is zero-based
            var day = date.getDate().toString().padStart(2, '0');
            return `${year}-${month}-${day}`;
        }

        function isTimeBooked(selectedDate, formattedTime) {
            // Filter bookings to include only those for the selected date
            var bookedTimes = bookingsStartTimes.filter(function(booking) {
                return booking.date === selectedDate;
            }).map(function(booking) {
                // Extract hours and minutes from the starttime string
                var startTimeParts = booking.starttime.split(':');
                var hours = parseInt(startTimeParts[0], 10);
                var minutes = parseInt(startTimeParts[1], 10);

                // Create a new Date object with the extracted hours and minutes
                var startTimeDate = new Date();
                startTimeDate.setHours(hours, minutes);

                // Format the Date object as H:i A
                var formattedStartTime = startTimeDate.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true });

                return formattedStartTime;
            });

            // Check if the formatted time (H:i A) is in the booked times array
            return bookedTimes.includes(formattedTime);
        }

        selectDateInput.addEventListener('change', generateTimeSlots);
        generateTimeSlots(); // Generate time slots initially
    });
</script> --}}
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var select = document.getElementById("slot-container");
        var selectDateInput = document.getElementById("appointment-date");
        var facilityStartTime = '{{ $facility->starttime }}';
        var facilityEndTime = '{{ $facility->endtime }}';
        var step = '{{ $facility->step }}'; // Step in H:i:s format (e.g., '00:30:00')
        var timeZone = 'Asia/Thimphu'; // Timezone for Bhutan (Asia/Thimphu)
        var bookingsStartTimes = @json($bookingsData);

        function generateTimeSlots() {
            select.innerHTML = '';

            var selectedDate = selectDateInput.value;
            var today = new Date(); // Current date and time
            var currentTime = new Date(today.toLocaleString('en-US', { timeZone: timeZone }));

            var startTime = new Date(selectedDate + 'T' + facilityStartTime);
            var endTime = new Date(selectedDate + 'T' + facilityEndTime);

            // Generate time slots
            var currentTimeIncremented = new Date(startTime);

            while (currentTimeIncremented < endTime) {
                var startTimeFormatted = currentTimeIncremented.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true, timeZone: timeZone });

                // Increment currentTimeIncremented by the step
                currentTimeIncremented.setHours(currentTimeIncremented.getHours() + parseInt(step.substring(0, 2))); // Add hours from step
                currentTimeIncremented.setMinutes(currentTimeIncremented.getMinutes() + parseInt(step.substring(3, 5))); // Add minutes from step
                currentTimeIncremented.setSeconds(currentTimeIncremented.getSeconds() + parseInt(step.substring(6, 8))); // Add seconds from step

                var endTimeFormatted = currentTimeIncremented.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true, timeZone: timeZone });

                // Skip displaying time slots that are in the past
                if (selectedDate === formatDate(today) && currentTimeIncremented < currentTime) {
                    continue;
                }

                // Check if the time slot is already booked for the selected date
                var isBooked = isTimeBooked(selectedDate, startTimeFormatted);

                // Only add the time slot if it's not booked
                if (!isBooked) {
                    var option = document.createElement('option');
                    option.text = `${startTimeFormatted} - ${endTimeFormatted}`;
                    option.value = startTimeFormatted;
                    select.appendChild(option);
                }
            }
        }

        function formatDate(date) {
            // Helper function to format date as YYYY-MM-DD
            var year = date.getFullYear();
            var month = (date.getMonth() + 1).toString().padStart(2, '0'); // Month is zero-based
            var day = date.getDate().toString().padStart(2, '0');
            return `${year}-${month}-${day}`;
        }

        function isTimeBooked(selectedDate, formattedTime) {
            // Filter bookings to include only those for the selected date
            var bookedTimes = bookingsStartTimes.filter(function(booking) {
                return booking.date === selectedDate;
            }).map(function(booking) {
                // Extract hours and minutes from the starttime string
                var startTimeParts = booking.starttime.split(':');
                var hours = parseInt(startTimeParts[0], 10);
                var minutes = parseInt(startTimeParts[1], 10);

                // Create a new Date object with the extracted hours and minutes
                var startTimeDate = new Date();
                startTimeDate.setHours(hours, minutes);

                // Format the Date object as H:i A
                var formattedStartTime = startTimeDate.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true });

                return formattedStartTime;
            });

            // Check if the formatted time (H:i A) is in the booked times array
            return bookedTimes.includes(formattedTime);
        }

        selectDateInput.addEventListener('change', generateTimeSlots);
        generateTimeSlots(); // Generate time slots initially
    });
</script>




</x-user-layout>
