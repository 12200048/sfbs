<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/booking.css')}}">
    <h1>Bookings</h1>
    <div class="maincontainer">
        <div class="row">
            <div class="d-flex">
                <div class="p-2 ms-auto">
                    <div class="dropdown">
                        <a class="btn btn-outline-secondary dropdown-toggle btn-sm" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Select User
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#" onclick="changeTableHeader('Student')">Student</a></li>
                            <li><a class="dropdown-item" href="#" onclick="changeTableHeader('Staff')">Staff</a></li>
                        </ul>
                    </div>
                </div>
                <div class=" p-2">
                    <div class="dropdown">
                        <a class="btn btn-outline-secondary dropdown-toggle btn-sm" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Requests
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">History</a></li>
                        </ul>
                    </div>
                </div>
                <div class=" p-2">
                    <div class="input-group input-group-sm mb-3">
                        <input type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search">
                        <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>
        </div>

        <div class=" table-responsive p-3">
            <table class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th id="headerLabel" scope="col" style="color:white; padding-Left:5px">Student ID</th>

                        <th scope="col" style="color:white; padding-Left:5px">Facility</th>

                        <th scope="col" style="color:white; padding-Left:5px">Date</th>
                        <th scope="col" style="color:white; padding-Left:5px">Time</th>
                        <th scope="col" style="color:white; padding-Left:5px">status</th>
                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                       $sl = 1; 
                    @endphp
                    @foreach ($bookings as $booking)
                        
                    
                    <tr>
                        <th scope="row">{{$sl}}</th>

                        <td style="font-size:12px; padding-Left:5px">{{$booking->user->employeeid}}</td>
                        <td style="font-size:12px; padding-Left:5px">{{$booking->facility->facility_name}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$booking->date}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$booking->starttime}}-{{$booking->endtime}}</td>
                        <td style="color:#27A806; font-size:12px; padding-Left:5px">{{$booking->status}}</td>
                        <td>

                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{$booking->id}}" data-bs-whatever="@getbootstrap">View</button>

                            <div class="modal fade" id="exampleModal{{$booking->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Booking Details</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Username</label>
                                                    <p>{{$booking->user->name}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">E-mail Id</label>
                                                    <p>{{$booking->user->email}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Student Id/employee Id</label>
                                                   <p>{{$booking->user->employeeid}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Date</label>
                                                    <p>{{$booking->date}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Time</label>
                                                    <p>{{$booking->starttime}}-{{$booking->endtime}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Facility Name</label>
                                                    <p>{{$booking->facility->facility_name}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="message-text" class="col-form-label">Reason for booking</label>
                                                    <p>{{$booking->reason}}</p>
                                                </div>
                                                
                                            </form>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <form action="{{route('bookings.deny')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                <input type="hidden" value="{{$booking->id}}" name="booking_id">
                                                <button type="submit" class="btn btn-primary">Deny</button>
                                            </form>
                                        
                                            <form action="{{route('bookings.approve')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                <input type="hidden" value="{{$booking->id}}" name="booking_id">
                                                <button type="submit" class="btn btn-primary">Approve</button>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </td>
                        @php
                            $sl++
                        @endphp
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination ">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        function changeTableHeader(option) {
            const headerLabel = document.getElementById('headerLabel');
            if (option === 'Student') {
                headerLabel.textContent = 'Student ID';
            } else if (option === 'Staff') {
                headerLabel.textContent = 'Staff ID';
            }
        }
    </script>

</x-app-layout>