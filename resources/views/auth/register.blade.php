<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/registration.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <title>Document</title>
</head>
<body>

    <div class="card1">
         <img class='bg' src="{{asset('img/bg.jpeg')}}"/>
         <div >
            <p class="text">Welcome Back</p>
            <p class="text1">
                Gain access to our facility booking system to easily <br> reserve spaces and amenities for your events <br> and activities. Log in now to manage your<br> bookings and streamline your <br>planning process.
            </p>
            <p class="text3">Already have an account?</p>
         
            <a class='button' href="{{route('login')}}">Login Now</a>

         </div>   
    </div>

    <div class="card">
    <img class="rlogo" src="{{asset('img/logo.png')}}" />
    <form action="{{route('register')}}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Name" required>
        <x-input-error :messages="$errors->get('name')"/>
        <input type="text" name="employeeid" placeholder="Employee ID/Student ID" required>
        <x-input-error :messages="$errors->get('employeeid')"/>
        <input type="email" name="email" placeholder="Email Address" required>
        <x-input-error :messages="$errors->get('email')"/>
        <input type="number" name="contact" placeholder="Contact Number">
        <x-input-error :messages="$errors->get('contact')"/>
        <select type="text" name="role" type="text" required>
            <option value="">Register as</option>
            <option value="student">Student</option>
            <option value="staff">Staff</option>
        </select>
        <x-input-error :messages="$errors->get('role')"/>
        <input type="password" name="password" placeholder="Password" required>
        <x-input-error :messages="$errors->get('password')"/>
        <input type="password" name="password_confirmation" placeholder="Confirm Password" required>
        <x-input-error :messages="$errors->get('password_confirmation')"/>
        <input type="submit" value="Register">
    </form>
    </div>
    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
</body>
</html>


