<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/booking.css')}}">
    <h1>Users</h1>
    <div class="maincontainer">
        <div class="row">
            <div class="d-flex">
                <div class="p-2 ms-auto">
                    <div class="dropdown">
                        <a class="btn btn-outline-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Select User
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#" onclick="changeTableHeader('Student')">Student</a></li>
                            <li><a class="dropdown-item" href="#" onclick="changeTableHeader('Staff')">Staff</a></li>
                        </ul>
                    </div>
                </div>
                <div class="p-2">
                    <div class="input-group input-group mb-3">
                        <input type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search">
                        <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class=" table-responsive p-3">
            <table class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th id="headerLabel" scope="col" style="color:white; padding-Left:5px">Student ID</th>

                        <th scope="col" style="color:white; padding-Left:5px">Username</th>

                        <th scope="col" style="color:white; padding-Left:5px">E-mail</th>
                        <th scope="col" style="color:white; padding-Left:5px">Contact.no</th>

                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $serialNumber = 1; // Initialize the serial number counter
                    @endphp
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{$serialNumber}}</th>
                        <td style=" font-size:12px; padding-Left:5px">{{$user->employeeid}}</td>
                        <td style="font-size:12px; padding-Left:5px">{{$user->name}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$user->email}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$user->contact}}</td>

                        <td>
                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Delete
                            </button>

                            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Delete Users</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this user?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <form action="{{ route('user.deleteUser', $user->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button style="color:rgb(218, 64, 64)" type="submit" class="btn btn-primary">Yes</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                    @php
                        $serialNumber++; // Increment the serial number for the next iteration
                    @endphp
                    @endforeach

                </tbody>
            </table>
            <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination ">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        function changeTableHeader(option) {
            const headerLabel = document.getElementById('headerLabel');
            if (option === 'Student') {
                headerLabel.textContent = 'Student ID';
            } else if (option === 'Staff') {
                headerLabel.textContent = 'Staff ID';
            }
        }
    </script>
</x-app-layout>