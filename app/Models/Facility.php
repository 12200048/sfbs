<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Booking;

class Facility extends Model
{
    use HasFactory;
    protected $fillable = [
        'facility_name',
        'desc',
        'starttime',
        'endtime',
        'step',
        'role',
        'image',
        'status'
    ];
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'facility_id');
    }
}
