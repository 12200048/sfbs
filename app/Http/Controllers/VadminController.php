<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Facility;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;


class VadminController extends Controller
{
    public function Dashboard(){
        $facilities = Facility::all();
        $rowCount = DB::table('facilities')->count();
        $countBook = DB::table('bookings')->count();
        $totalUsers = DB::table('users')->count();
        // Fetch bookings associated with the current user
        $bookings = Booking::with('facility','users')->get();

        return view('dashboard',compact('facilities','rowCount','countBook','totalUsers','bookings'));
    }
    
    public function Bookings(){
        $bookings = Booking::all();
        return view('bookings',compact('bookings'));
    }
    public function bookingsApprove(Request $request)
    {
        // Validate input data (booking ID)
        $request->validate([
            'booking_id' => 'required|exists:bookings,id',
        ]);

        // Get the booking ID from the request
        $bookingId = $request->input('booking_id');

        // Find the booking by ID
        $booking = Booking::findOrFail($bookingId);

        // Update the status to 'Accepted' (assuming 'status' is a field in your Booking model)
        $booking->status = 'Accepted';
        $booking->save();

        // Redirect back with success message
        return redirect()->back()->with('success', 'Booking approved successfully!');
    }
    public function Facilities(){
        $facilities = Facility::all();
        return view('facilities', compact('facilities'));
    }
    public function FacilitiesCreate(Request $request){
        try {
            // Validate the incoming request data
            $validatedData = $request->validate([
                'facility_name' => ['required', 'string', 'unique:facilities'],
                'desc' => ['required', 'string'],
                'starttime' => ['required', 'date_format:H:i'],
                'endtime' => ['required', 'date_format:H:i'],
                'hour' => ['required', 'numeric', 'between:0,24'],
                'minute' => ['required', 'numeric', 'between:0,59'],
                'role' => ['required', 'string', 'in:all,staff'],
                'status' => ['required', 'string', 'in:available,unavailable'],
                'image' => ['required', 'image', 'mimes:jpeg,png'],
            ]);

            // Process the hour and minute inputs to create a formatted step value
            $hour = $request->input('hour');
            $minute = $request->input('minute');
            $step = sprintf('%02d:%02d:00', $hour, $minute);

            // Handle file upload (image)
            if ($request->hasFile('image')) {
                $filename = $request->file('image')->store('public');
                $filename = str_replace('public/', '', $filename);
            } else {
                throw new \Exception('Image file is required.'); // Throw exception if image is not provided
            }

            // Create a new Facility record using the validated and processed data
            Facility::create([
                'facility_name' => $validatedData['facility_name'],
                'desc' => $validatedData['desc'],
                'starttime' => $validatedData['starttime'],
                'endtime' => $validatedData['endtime'],
                'role' => $validatedData['role'],
                'image' => $filename,
                'step' => $step,
                'status' => $validatedData['status'],
            ]);

            // Redirect back with a success message
            return redirect()->back()->with('success', 'Facility added successfully!');
        } catch (ValidationException $e) {
            // Validation errors occurred
            return redirect()->back()->withErrors($e->validator->errors())->withInput();
        } catch (\Exception $e) {
            // Other exceptions (e.g., file upload error, database error)
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
     
    }

    public function BookingsDeny(Request $request){
         // Validate input data (booking ID)
         $request->validate([
            'booking_id' => 'required|exists:bookings,id|max:255',
        ]);

        // Get the booking ID from the request
        $bookingId = $request->input('booking_id');

        // Find the booking by ID
        $booking = Booking::findOrFail($bookingId);

        // Update the status to 'Accepted' (assuming 'status' is a field in your Booking model)
        $booking->status = 'Denied';
        $booking->save();

        // Redirect back with success message
        return redirect()->back()->with('success', 'Booking Denied Successfully!');
    }
    public function Feedbacks(){
        $feedbacks = Feedback::all();
        return view('feedbacks',compact('feedbacks'));
    }
    function DeleteFeedback($id){
        $feedback = Feedback::findOrFail($id); // Find the item by its ID

        $feedback->delete(); // Delete the item

        return redirect()->back()->with('success', 'Feedback deleted successfully');
    }
    
    public function User(){
        $users = User::all();

        return view('user',compact('users'));
    }
    public function Profile(){
        return view('profile');
    }
    function DeleteUser($id){
        $user = User::findOrFail($id); // Find the item by its ID

        $user->delete(); // Delete the item

        return redirect()->back()->with('success', 'Item deleted successfully');
    }
}
