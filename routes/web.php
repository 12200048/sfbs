<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VadminController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('userlogin');
// })->name('userlogin');



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
});

Route::middleware('auth', 'admin')->group(function(){
    Route::get('/dashboard', [VadminController::class, 'Dashboard'])->name('dashboard');
    Route::get('/bookings', [VadminController::class, 'Bookings'])->name('bookings');
    Route::patch('/bookings/approve', [VadminController::class, 'BookingsApprove'])->name('bookings.approve');
    Route::patch('/bookings/deny', [VadminController::class, 'BookingsDeny'])->name('bookings.deny');


    Route::get('/facilities', [VadminController::class, 'Facilities'])->name('facilities');
    Route::post('/facilities/create', [VadminController::class, 'FacilitiesCreate'])->name('facilities.create');

    

    Route::get('/feedbacks', [VadminController::class, 'Feedbacks'])->name('feedbacks');
    Route::delete('/feedbacks/deletefeedback/{id}',[VadminController::class, 'DeleteFeedback'])->name('user.deleteFeedback');

    Route::get('/user', [VadminController::class, 'User'])->name('user');
    Route::delete('/user/deleteuser/{id}',[VadminController::class, 'DeleteUser'])->name('user.deleteUser');

    Route::get('/profile', [VadminController::class, 'Profile'])->name('profile');

});

Route::middleware('auth', 'user')->group(function(){
    Route::get('/user/homepage',[UserController::class, 'Homepage'])->name('user.homepage');
    Route::get('/user/bookingpage/{id}',[UserController::class, 'Bookingpage'])->name('user.bookingpage');
    Route::post('/user/bookingpage/create',[UserController::class, 'BookingpageCreate'])->name('user.bookingpage.create');
    Route::post('/user/feedback',[UserController::class, 'FeedbackPost'])->name('feedback.store');

    Route::get('/user/feedback',[UserController::class, 'Feedback'])->name('user.feedback');
    Route::get('/user/mybooking',[UserController::class, 'Mybooking'])->name('user.mybooking');
    Route::delete('/user/deletebooking/{id}',[UserController::class, 'DeleteBooking'])->name('user.deleteBooking');

    Route::get('/user/profile',[UserController::class, 'Profile'])->name('user.profile');
    Route::put('/user/{user}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('users.destroy');

});


require __DIR__.'/auth.php';
